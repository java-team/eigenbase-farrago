#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
PACKAGE=$(dpkg-parsechangelog -S Source)
VERSION=$2
DIR=${PACKAGE}-${VERSION}
TAR=../${PACKAGE}_${VERSION}.orig.tar.xz

# Repack upstream source to tar.gz
tar xjf $3
mv farrago-* $DIR
XZ_OPT=--best tar cJf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
